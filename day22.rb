def spells
  {
    'magic_missile' => { 'cost' => 53, 'instant' => {'damage' => 4, 'heal' => 0} },
    'drain' => { 'cost' => 73, 'instant' => {'damage' => 2, 'heal' => 2} },
    'shield' => { 'cost' => 113, 'effect' => {'damage' => 0, 'armor_bonus' => 7, 'mana_restore' => 0, 'duration' => 6} },
    'poison' => { 'cost' => 173, 'effect' => {'damage' => 3, 'armor_bonus' => 0, 'mana_restore' => 0, 'duration' => 6 } },
    'recharge' => { 'cost' => 229, 'effect' => {'damage' => 0, 'armor_bonus' => 0, 'mana_restore' => 101, 'duration' => 5} }
  }
end

def player_stats
  {
    'hit_points' => 50,
    'mana_points' => 500
  }
end

def boss_stats
  {
    'hit_points' => 58,
    'damage' => 9
  }
end

def player_wins?(list_of_spells, hard_mode = false)
  # puts list_of_spells.inspect

  boss_hp = boss_stats['hit_points']
  player_hp = player_stats['hit_points']
  player_mana = player_stats['mana_points']
  player_armor = 0

  poison_duration = 0
  recharge_duration = 0
  shield_duration = 0

  list_of_spells.each do |spell_name|
    spell = spells[spell_name]

    if hard_mode
      player_hp -= 1 if hard_mode
      return :false if player_hp <= 0
    end

    #player turn
    if recharge_duration > 0
      player_mana += 101
      recharge_duration -= 1
    end

    if shield_duration > 0
      player_armor = 7
      shield_duration -= 1
    else
      player_armor = 0
    end

    if poison_duration > 0
      boss_hp -= 3
      poison_duration -= 1
      return :true if boss_hp <= 0
    end

    # puts "mana: #{player_mana}"
    return :false if player_mana < 53

    player_mana -= spell['cost']
    if spell.has_key?('instant')
      boss_hp -= spell['instant']['damage']
      player_hp += spell['instant']['heal']
      return :true if boss_hp <= 0
    elsif spell.has_key?('effect')
      if spell_name == 'recharge'
        recharge_duration = 5
      elsif spell_name == 'shield'
        shield_duration = 6
      elsif spell_name == 'poison'
        poison_duration = 6
      end
    end

    #boss turn
    if recharge_duration > 0
      player_mana += 101
      recharge_duration -= 1
    end

    if shield_duration > 0
      player_armor = 7
      shield_duration -= 1
    else
      player_armor = 0
    end

    if poison_duration > 0
      boss_hp -= 3
      poison_duration -= 1
      return :true if boss_hp <= 0
    end

    player_hp -= [boss_stats['damage'] - player_armor, 1].max
    return :false if player_hp <= 0
  end

  # puts "player: #{player_hp}"
  # puts "boss: #{boss_hp}"
  return :undecided
end

def mana_after_combo(combo)
  recharge_duration = 0
  player_mana = player_stats['mana_points']
  combo.each do |spell_name|
    if recharge_duration > 0
      player_mana += 101
      recharge_duration -= 1
    end

    player_mana -= spells[spell_name]['cost']
    recharge_duration = 5 if spell_name == 'recharge'

    if recharge_duration > 0
      player_mana += 101
      recharge_duration -= 1
    end
  end
  player_mana
end

def spells_off_cooldown_after_combo(combo)
  durations = {
    'shield' => 0,
    'poison' => 0,
    'recharge' => 0
  }

  combo.each do |spell_name|
    durations.each do |k, v|
      durations[k] = v - 1
    end

    if spells[spell_name].has_key?('effect')
      durations[spell_name] = spells[spell_name]['effect']['duration']
    end

    durations.each do |k, v|
      durations[k] = v - 1
    end
  end

  durations.each do |k, v|
    durations[k] = v - 1
  end

  ['magic_missile', 'drain'] + durations.select { |k, v| v <= 0 }.keys
end

def available_spells_after_combo(combo)
  available_spells = []
  spells_off_cooldown_after_combo(combo).each do |spell|
    available_spells << spell if mana_after_combo(combo) - spells[spell]['cost'] >= 0
  end
  available_spells
end

def new_combos(combo)
  new_combos = []
  spells_off_cooldown_after_combo(combo).each do |spell|
    new_combos << combo + [spell] if mana_after_combo(combo) - spells[spell]['cost'] >= 0
  end
  new_combos
end

def mana_usage(combo)
  combo.map { |spell| spells[spell]['cost'] }.inject(:+)
end

def winning_spell_combo(hard_mode = false)
  best_combo = []
  least_mana = 100000000000000000000
  potential_combos = spells.keys.map { |spell| [spell] }

  until potential_combos.empty?
    combo = potential_combos.shift
    combo_mana_usage = mana_usage(combo)
    next if least_mana <= combo_mana_usage

    result = player_wins?(combo, hard_mode)
    if result == :true
      best_combo = combo
      least_mana = combo_mana_usage
    elsif result == :undecided
      potential_combos += new_combos(combo)
    end
  end

  best_combo
end

def part_one_solution
  mana_usage(winning_spell_combo)
end

def part_two_solution
  mana_usage(winning_spell_combo(true))
end

def monte_carlo_approach(repeat)
  best_score = 10000000000
  repeat.times do |i|
    combo = []
    finished = false
    until finished
      result = player_wins?(combo)
      if result == :true
        mana_used = mana_usage(combo)
        best_score = mana_used if mana_used < best_score
        finished = true
      elsif result == :false
        finished = true
      else
        available_spells = available_spells_after_combo(combo)
        if available_spells.empty?
          finished = true
        else
          combo << available_spells_after_combo(combo).sample
        end
      end
    end
  end
  best_score
end
