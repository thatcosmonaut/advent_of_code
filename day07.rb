require 'graphunk'

#utility
def input
  File.readlines("inputs/day07.txt").map(&:strip)
end

class OneInputGate
  def initialize(input, output)
    @input = input
    @output = output

    @input.add_output_gate(self)
    @output.set_input_gate(self)
  end

  def output
    @output
  end

  def compute
    nil
  end

  def output_signal
    if !@input.value.nil? && @output.value.nil?
      @output.set_value(compute)
    end
  end

  def operation_string
    ''
  end
end

class TwoInputGate
  def initialize(input_one, input_two, output)
    @input_one = input_one
    @input_two = input_two
    @output = output

    @input_one.add_output_gate(self)
    @input_two.add_output_gate(self)
    @output.set_input_gate(self)
  end

  def compute
    nil
  end

  def output
    @output
  end

  def output_signal
    if !@input_one.value.nil? && !@input_two.value.nil? && @output.value.nil?
      @output.set_value(compute)
    end
  end
end

class AndGate < TwoInputGate
  def compute
    @input_one.value & @input_two.value
  end

  def operation_string
    @input_one.name + ' && ' + @input_two.name + ' -> ' + @output.name
  end
end

class OrGate < TwoInputGate
  def compute
    @input_one.value | @input_two.value
  end

  def operation_string
    @input_one.name + ' || ' + @input_two.name + ' -> ' + @output.name
  end
end

class NotGate < OneInputGate
  def compute
    ~@input.value
  end

  def operation_string
    '~' + @input.name + ' -> ' +  @output.name
  end
end

class LeftShiftGate < TwoInputGate
  def compute
    @input_one.value << @input_two.value
  end

  def operation_string
    @input_one.name + ' << ' + @input_two.name + ' -> ' + @output.name
  end
end

class RightShiftGate < TwoInputGate
  def compute
    @input_one.value >> @input_two.value
  end

  def operation_string
    @input_one.name + ' >> ' + @input_two.name + ' -> ' + @output.name
  end
end

class AssignmentGate < OneInputGate
  def initialize(input_wire, output_wire)
    @input = input_wire
    @output = output_wire

    @input.add_output_gate(self)
    @output.set_input_gate(self)
  end

  def compute
    @input.value
  end

  def operation_string
    @input.name + ' -> ' + @output.name
  end
end

#values are 16-bit
class Wire
  def initialize(name, initial_value = nil)
    @name = name
    @input_gate = nil
    @output_gates = []
    set_value(initial_value) unless initial_value.nil?
  end

  def set_value(value)
    @value = force_overflow_unsigned(value.to_i)
  end

  def value
    @value
  end

  def reset_value
    @value = nil
  end

  def set_input_gate(given_input_gate)
    @input_gate = given_input_gate
  end

  def add_output_gate(given_output_gate)
    @output_gates << given_output_gate
  end

  def input_gate
    @input_gate
  end

  def output_gates
    @output_gates
  end

  def name
    @name.nil? ? '' : @name
  end

  private

  def force_overflow_unsigned(i)
    i & 0xffffffff
  end
end

class Circuit
  def initialize(verbose = false)
    @wires = {}
    @gates = []
    @gate_names = {}
    @gate_lookup = {}
    @verbose = verbose
  end

  def parse_line(string)
    if and?(string)
      parse_and_connect_and_gate(string)
    elsif or?(string)
      parse_and_connect_or_gate(string)
    elsif not?(string)
      parse_and_connect_not_gate(string)
    elsif left_shift?(string)
      parse_and_connect_left_shift_gate(string)
    elsif right_shift?(string)
      parse_and_connect_right_shift_gate(string)
    elsif assignment?(string)
      parse_and_connect_assignment_gate(string)
    end
  end

  def wires
    @wires
  end

  def gates
    @gates
  end

  def order
    counter = 0
    topo_graph = Graphunk::DirectedGraph.new

    @gates.each do |gate|
      vertex_name = gate.class.name + counter.to_s
      topo_graph.add_vertex(vertex_name)
      @gate_names[gate] = vertex_name
      @gate_lookup[vertex_name] = gate
      counter += 1
    end

    @wires.each do |name, wire|
      wire.output_gates.each do |output_gate|
        topo_graph.add_edge(@gate_names[wire.input_gate], @gate_names[output_gate])
      end
    end

    topo_graph.topological_sort
  end

  def order_formatted
    order.map { |gate_name| @gate_lookup[gate_name] }.map { |gate| gate.operation_string + "\n" }.inject(:+)
  end

  def run
    gate_order = order.map { |gate_name| @gate_lookup[gate_name] }
    gate_order.each do |gate|
      gate.output_signal
      if @verbose
        puts gate.operation_string
        puts gate.output.value
      end
    end
    return
  end

  def reset_wire_values
    @wires.each { |name, wire| wire.reset_value }
    return
  end

  private

  def and?(string)
    !(string =~ /AND/).nil?
  end

  def or?(string)
    !(string =~ /OR/).nil?
  end

  def not?(string)
    !(string =~ /NOT/).nil?
  end

  def left_shift?(string)
    !(string =~ /LSHIFT/).nil?
  end

  def right_shift?(string)
    !(string =~ /RSHIFT/).nil?
  end

  def assignment?(string)
    string.scan(/[a-z\d]+/).length == 2
  end

  def parse_and_connect_and_gate(string)
    wire_one_string, wire_two_string, output_wire_string = string.scan(/[a-z\d]+/)
    input_wire_one = find_or_create_wire(wire_one_string)
    input_wire_two = find_or_create_wire(wire_two_string)
    output_wire = find_or_create_wire(output_wire_string)

    @gates << AndGate.new(input_wire_one, input_wire_two, output_wire)
  end

  def parse_and_connect_or_gate(string)
    wire_one_string, wire_two_string, output_wire_string = string.scan(/[a-z\d]+/)
    input_wire_one = find_or_create_wire(wire_one_string)
    input_wire_two = find_or_create_wire(wire_two_string)
    output_wire = find_or_create_wire(output_wire_string)

    @gates << OrGate.new(input_wire_one, input_wire_two, output_wire)
  end

  def parse_and_connect_not_gate(string)
    input_wire_string, output_wire_string = string.scan(/[a-z\d]+/)
    input_wire = find_or_create_wire(input_wire_string)
    output_wire = find_or_create_wire(output_wire_string)

    @gates << NotGate.new(input_wire, output_wire)
  end

  def parse_and_connect_left_shift_gate(string)
    input_wire_string, constant_wire_string, output_wire_string = string.scan(/[a-z\d]+/)
    input_wire = find_or_create_wire(input_wire_string)
    constant_wire = Wire.new(constant_wire_string, constant_wire_string)
    output_wire = find_or_create_wire(output_wire_string)

    @gates << LeftShiftGate.new(input_wire, constant_wire, output_wire)
  end

  def parse_and_connect_right_shift_gate(string)
    input_wire_string, constant_wire_string, output_wire_string = string.scan(/[a-z\d]+/)
    input_wire = find_or_create_wire(input_wire_string)
    constant_wire = Wire.new(constant_wire_string, constant_wire_string)
    output_wire = find_or_create_wire(output_wire_string)

    @gates << RightShiftGate.new(input_wire, constant_wire, output_wire)
  end

  def parse_and_connect_assignment_gate(string)
    input_wire_string, output_wire_string = string.scan(/[a-z\d]+/)
    input_wire = find_or_create_wire(input_wire_string)
    output_wire = find_or_create_wire(output_wire_string)

    assignment_gate = AssignmentGate.new(input_wire, output_wire)
    @gates << assignment_gate
  end

  def find_or_create_wire(string)
    if is_int?(string)
      Wire.new(string, string)
    elsif @wires.has_key?(string)
      @wires[string]
    else
      @wires[string] = Wire.new(string)
    end
  end

  def is_int?(string)
    !(string =~ /\d+/).nil?
  end
end

def part_one_solution
  circuit = Circuit.new
  input.each do |line|
    circuit.parse_line(line)
  end
  circuit.run
  circuit.wires['a'].value
end

def part_two_solution
  circuit = Circuit.new
  input.each do |line|
    circuit.parse_line(line)
  end
  circuit.run
  a_value = circuit.wires['a'].value
  circuit.reset_wire_values
  circuit.wires['b'].set_value(a_value)
  circuit.run
  circuit.wires['a'].value
end

def circuit_order
  circuit = Circuit.new
  input.each do |line|
    circuit.parse_line(line)
  end
  circuit.order_formatted
end
