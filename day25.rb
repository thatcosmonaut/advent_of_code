def row_value(row)
  add_value = 1
  value = 1
  (row-1).times do
    value += add_value
    add_value += 1
  end
  value
end

def increment_value(row)
  row+1
end

def index_value(row, column)
  value = row_value(row)
  add = increment_value(row)
  (column-1).times do
    value += add
    add += 1
  end
  value
end

def part_one_solution
  value = 20151125
  (index_value(3010, 3019)-1).times do
    value = (value * 252533) % 33554393
  end
  value
end
