def input
  File.open("inputs/day01.txt").read.strip
end

#part one
def parens_to_floor_num
  input.split("").map { |char| char == "(" ? 1 : -1 }.inject(:+)
end

#part two
def basement_entry_position
  floor = 0
  position = 0
  chars = input.split("")
  while position < chars.length
    if chars[position] == "("
      floor += 1
    else
      floor -= 1
      return position + 1 if floor == -1 #solution is 1-indexed for some reason
    end
    position += 1
  end

  return nil
end
