def input
  File.readlines("inputs/day17.txt").map(&:strip).map(&:to_i)
end

#there needs to be support for individual duplicates.....................
#
def change(n, list_of_coins)
  change_helper(n, list_of_coins, [])
end

def change_helper(n, list_of_coins, result_list)
  if n == 0
    return [result_list]
  elsif list_of_coins.empty?
    return []
  else
    list_of_coins.each_with_index do |coin, index|
      if n < coin
        return []
      else
        return change_helper(n - coin, remove_index(list_of_coins, index), result_list + [coin]) + change_helper(n, remove_index(list_of_coins, index), result_list)
      end
    end
  end
end

def remove_index(list, index)
  if list.length < 1
    []
  else
    if index == 0
      list[1..list.length-1]
    elsif index == list.length-1
      list[0..list.length-2]
    else
      list[0..index-1] + list[index+1..list.length-1]
    end
  end
end

def part_one_solution
  change(150, input.sort).length
end

def part_two_solution
  different_ways = change(150, input.sort)
  min_amount = different_ways.map(&:length).min
  different_ways.select { |list| list.length == min_amount }.length
end
