def part_one_solution
  house_num = 34000000
  size = house_num / 10

  houses = Array.new(size, 0)
  (1..size-1).each do |i|
    (i..size-1).step(i).each do |j|
      houses[j] += i * 10
    end
  end

  houses.find_index { |x| x >= house_num }
end

def part_two_solution
  house_num = 34000000
  size = house_num / 10

  houses = Array.new(size, 0)
  (1..size-1).each do |i|
    (i..[i * 50, size-1].min).step(i).each do |j|
      houses[j] += i * 11
    end
  end

  houses.find_index { |x| x >= house_num }
end
