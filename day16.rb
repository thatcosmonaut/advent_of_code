def input
  File.readlines("inputs/day16.txt").map(&:strip)
end

def initialize_sues
  Array.new(500, Hash.new)
end

def parse_input
  sue_data = []
  input.each do |line|
    sue_index, key_one, value_one, key_two, value_two, key_three, value_three = line.scan(/\b[a-z]+\b|-?\d+/)
    sue_data << { key_one => value_one.to_i, key_two => value_two.to_i, key_three => value_three.to_i}
  end
  sue_data
end

def init_given_sue
  {
    'children' => 3,
    'cats' => 7,
    'samoyeds' => 2,
    'pomeranians' => 3,
    'akitas' => 0,
    'vizslas' => 0,
    'goldfish' => 5,
    'trees' => 3,
    'cars' => 2,
    'perfumes' => 1
  }
end

def part_one_solution
  sue_data = parse_input
  given_sue = init_given_sue

  sue_data.index do |sue|
    sue.select { |k,v| given_sue[k] == v }.length == 3
  end + 1
end

def part_two_solution
  sue_data = parse_input
  given_sue = init_given_sue

  sue_data.index do |sue|
    sue.select do |k,v|
      if k == 'cats' || k == 'trees'
        given_sue[k] < v
      elsif k == 'pomeranians' || k == 'goldfish'
        given_sue[k] > v
      else
        given_sue[k] == v
      end
    end.length == 3
  end + 1
end
