def input
  File.readlines("inputs/day08.txt").map(&:strip)
end

def characters_in_string_code(string)
  string.length
end

def characters_in_string_memory(string)
  replace_escape_sequences(string).length - 2
end

def replace_escape_sequences(string)
  string.gsub(/\\\\/, 'Z').gsub(/\\x[0-9a-f][0-9a-f]/, 'X').gsub(/\\"/, 'Y')
end

def part_one_solution
  input.map { |line| characters_in_string_code(line) - characters_in_string_memory(line) }.inject(:+)
end

def characters_in_encoded_string(string)
  string.dump.length
end

def part_two_solution
  input.map { |line| characters_in_encoded_string(line) - characters_in_string_code(line) }.inject(:+)
end
