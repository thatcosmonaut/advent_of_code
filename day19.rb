def input
  File.readlines("inputs/day19.txt").map(&:strip)
end

def parse_rules
  rules = []
  input[0..-3].each do |line|
    elt_one, elt_two = line.scan(/[a-zA-Z]+/)
    rules << [elt_one, elt_two]
  end
  rules
end

def parse_medicine_molecule
  input.last
end

def separate_elements(start)
  start.scan(/[A-Z][a-z]?/)
end

def find_all_replacements(start, rules)
  exploded_start = separate_elements(start)
  replacements = []
  exploded_start.each_with_index do |elt, i|
    rules.each do |pair|
      if elt == pair.first
        new_molecule = exploded_start.clone
        new_molecule[i] = pair.last
        replacements << new_molecule.join('')
      end
    end
  end
  replacements
end

def part_one_solution
  find_all_replacements(parse_medicine_molecule, parse_rules).uniq.length
end

def tokens_count(string)
  separate_elements(string).count
end

def parens_count(string)
  string.scan(/Rn|Ar/).count
end

def comma_count(string)
  string.scan(/Y/).count
end

def steps(string)
  tokens_count(string) - parens_count(string) - 2*comma_count(string) - 1
end

def part_two_solution
  steps(parse_medicine_molecule)
end
