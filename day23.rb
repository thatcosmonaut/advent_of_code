def input
  File.readlines("inputs/day23.txt").map(&:strip)
end

class MachineEmulator
  attr_accessor :a, :b

  def initialize(a = 0, b = 0, instructions)
    @a = a
    @b = b

    @instructions = instructions
    @line_counter = 0
  end

  def parse_instruction(instruction)
    instruction, register_string, input_two_string = instruction.scan(/\b[a-z]+\b|-?\d+/)

    register_var = "@#{register_string}"

    if half?(instruction)
      instance_variable_set(register_var, instance_variable_get(register_var) * 0.5)
      @line_counter += 1
    elsif triple?(instruction)
      instance_variable_set(register_var, instance_variable_get(register_var) * 3)
      @line_counter += 1
    elsif increment?(instruction)
      instance_variable_set(register_var, instance_variable_get(register_var) + 1)
      @line_counter += 1
    elsif jump?(instruction)
      @line_counter += register_string.to_i
    elsif jump_if_even?(instruction)
      if instance_variable_get(register_var) % 2 == 0
        @line_counter += input_two_string.to_i
      else
        @line_counter += 1
      end
    elsif jump_if_one?(instruction)
      if instance_variable_get(register_var) == 1
        @line_counter += input_two_string.to_i
      else
        @line_counter += 1
      end
    end
  end

  def run
    @line_counter = 0
    while @line_counter < @instructions.count
      parse_instruction(@instructions[@line_counter])
    end
  end

  private

  def half?(instruction)
    instruction == 'hlf'
  end

  def triple?(instruction)
    instruction == 'tpl'
  end

  def increment?(instruction)
    instruction == 'inc'
  end

  def jump?(instruction)
    instruction == 'jmp'
  end

  def jump_if_even?(instruction)
    instruction == 'jie'
  end

  def jump_if_one?(instruction)
    instruction == 'jio'
  end
end

def part_one_solution
  emulator = MachineEmulator.new(input)
  emulator.run
  emulator.b
end

def part_two_solution
  emulator = MachineEmulator.new(1, 0, input)
  emulator.run
  emulator.b
end
