def input
  File.readlines("inputs/day09.txt").map(&:strip)
end

class ShortestPathSolver
  def initialize
    @cities = []
    @distances = {}
  end

  def parse_line(line)
    cities = line.slice(0..(line.index(' =') - 1))
    city_one, city_two = cities.split(" to ")

    distance = line.slice(line.index('= ')+2..line.length-1).to_i
    add_distance(city_one, city_two, distance)
  end

  def add_distance(city_one, city_two, distance)
    add_city(city_one) unless @cities.include?(city_one)
    add_city(city_two) unless @cities.include?(city_two)
    @distances[[city_one, city_two]] = distance
    @distances[[city_two, city_one]] = distance
  end

  def add_city(city)
    @cities << city
  end

  def find_shortest_distance
    possible_distances.min
  end

  def find_longest_distance
    possible_distances.max
  end

  private

  def possible_distances
    @cities.permutation.map do |ordering|
      ordering.each_cons(2).map do |cities|
        @distances[cities]
      end.inject(:+)
    end
  end

end

def generate_solver
  solver = ShortestPathSolver.new
  input.each do |line|
    solver.parse_line(line)
  end
  solver
end

def part_one_solution
  generate_solver.find_shortest_distance
end

def part_two_solution
  generate_solver.find_longest_distance
end
