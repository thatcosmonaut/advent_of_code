#utility
def input
  File.readlines("inputs/day06.txt").map(&:strip)
end

#part one
class LightGrid
  def initialize(x_dimension, y_dimension)
    @grid = Array.new(x_dimension) { Array.new(y_dimension, false) }
  end

  def toggle_through(start_i, start_j, end_i, end_j)
    (start_i..end_i).each do |i|
      (start_j..end_j).each do |j|
        @grid[i][j] = !@grid[i][j]
      end
    end
  end

  def turn_on_through(start_i, start_j, end_i, end_j)
    (start_i..end_i).each do |i|
      (start_j..end_j).each do |j|
        @grid[i][j] = true
      end
    end
  end

  def turn_off_through(start_i, start_j, end_i, end_j)
    (start_i..end_i).each do |i|
      (start_j..end_j).each do |j|
        @grid[i][j] = false
      end
    end
  end

  def parse_instruction(string)
    if string.start_with?("toggle")
      parse_and_execute_toggle(string)
    elsif string.start_with?("turn on")
      parse_and_execute_turn_on(string)
    elsif string.start_with?("turn off")
      parse_and_execute_turn_off(string)
    end
  end

  def lights_turned_on_count
    @grid.map { |list| list.count(true) }.inject(:+)
  end

  private

  def parse_indices(string)
    start_indices_string = string.scan(/\d+,\d+/)[0]
    end_indices_string = string.scan(/\d+,\d+/)[1]
    start_i, start_j = start_indices_string.split(",").map(&:to_i)
    end_i, end_j = end_indices_string.split(",").map(&:to_i)
    [start_i, start_j, end_i, end_j]
  end

  def parse_and_execute_toggle(string)
    indices = parse_indices(string)
    toggle_through(indices[0], indices[1], indices[2], indices[3])
  end

  def parse_and_execute_turn_on(string)
    indices = parse_indices(string)
    turn_on_through(indices[0], indices[1], indices[2], indices[3])
  end

  def parse_and_execute_turn_off(string)
    indices = parse_indices(string)
    turn_off_through(indices[0], indices[1], indices[2], indices[3])
  end
end

def part_one_solution
  light_grid = LightGrid.new(1000,1000)
  input.each do |line|
    light_grid.parse_instruction(line)
  end
  light_grid.lights_turned_on_count
end

#part two
class BrightnessLightGrid < LightGrid
  def initialize(x_dimension, y_dimension)
    @grid = Array.new(x_dimension) { Array.new(y_dimension, 0) }
  end

  def toggle_through(start_i, start_j, end_i, end_j)
    (start_i..end_i).each do |i|
      (start_j..end_j).each do |j|
        @grid[i][j] += 2
      end
    end
  end

  def turn_on_through(start_i, start_j, end_i, end_j)
    (start_i..end_i).each do |i|
      (start_j..end_j).each do |j|
        @grid[i][j] += 1
      end
    end
  end

  def turn_off_through(start_i, start_j, end_i, end_j)
    (start_i..end_i).each do |i|
      (start_j..end_j).each do |j|
        @grid[i][j] = [@grid[i][j] - 1, 0].max
      end
    end
  end

  def total_brightness
    @grid.map { |list| list.inject(:+) }.inject(:+)
  end
end

def part_two_solution
  light_grid = BrightnessLightGrid.new(1000,1000)
  input.each do |line|
    light_grid.parse_instruction(line)
  end
  light_grid.total_brightness
end
